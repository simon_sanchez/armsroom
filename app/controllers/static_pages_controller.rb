class StaticPagesController < ApplicationController
  
  def home
    if logged_in?
      @equip = current_user.equips.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def about
  end
end
