class EquipsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @equip = current_user.equips.build(equip_params)
    if @equip.save
      flash[:success] = "Equipment created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @equip.destroy
    flash[:success] = "Equipment removed"
    redirect_to request.referrer || root_url
  end
  
  private

    def equip_params
      params.require(:equip).permit(:name)
    end
    
    def correct_user
      @equip = current_user.equips.find_by(id: params[:id])
      redirect_to root_url if @equip.nil?
    end
    
end