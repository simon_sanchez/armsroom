class AddIndexToUnit < ActiveRecord::Migration
  def change
    add_index :users, :unit, unique: false
  end
end
