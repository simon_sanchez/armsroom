class CreateEquips < ActiveRecord::Migration
  def change
    create_table :equips do |t|
      t.text :name
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :equips, :users
  end
end
